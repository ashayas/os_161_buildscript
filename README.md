# OS 161 Buildscript #
Build script for OS_161 that will:
* *bmake* dependencies
* *bmake* 
* *bmake install*
* run your kernel
It **does** stop execution on a failure of one of the commands e.g. 
if your *bmake* fails and you can read the corresponding output of bmake 

It does require your file directory to be similar to mine in that:

`os161/src`
Contains all your src files 

### Setup
* Clone the repostitory and place in your src folder
* Add these files to your gitignore

`chmod +x buildscript.sh`

`bash ./buildscript.sh`

### Debug Mode

* You can also run this in debug mode with either of

`bash ./buildscript.sh debug`

`bash ./buildscript.sh -w`